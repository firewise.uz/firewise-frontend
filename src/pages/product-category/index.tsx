import Layout from "../../layout";
import "./index.css";
import { useNavigate } from "react-router-dom";

interface IData {
  id: number;
  title: string;
  desc: string;
}

const data: IData[] = [
  {
    id: 1,
    title: "ЭКСПОРТНАЯ ПРОДУКЦИЯ",
    desc: "Имеющая ряд национальных и международных сертификатов соответствия стандартам, таким как международный UL, глобальный LPCB, европейский CE, китайский CCC, российский РСТ, узбекский O’zDSt идр.",
  },
  {
    id: 2,
    title: "БЮДЖЕТНАЯ ПРОДУКЦИЯ",
    desc: "Имеющая ряд национальных и международных сертификатов соответствия стандартам, таким как международный UL, глобальный LPCB, европейский CE, китайский CCC, узбекский O’zDSt и др.",
  },
];

const ProductCategory = () => {
  const navigate = useNavigate();

  const selectCountry = (id: string) => {
    if (id) {
      localStorage.setItem("countryid", id);
      navigate("/products");
    }
  };

  return (
    <Layout>
      <div className="category_wrapper">
        <div className="category_card_wrapper">
          {data.map(({ desc, id, title }: IData) => (
            <div
              className="category_card_item"
              onClick={() => selectCountry(`${id}`)}
              style={{ width: "100%", padding: "20px" }}
            >
              <h3
                className="linear_gradient_title__light"
                style={{
                  textAlign: "center",
                }}
              >
                {title}
              </h3>
              <p
                style={{
                  fontSize: "20px",
                }}
              >
                {desc}
              </p>
            </div>
          ))}
        </div>
      </div>
    </Layout>
  );
};
export default ProductCategory;
