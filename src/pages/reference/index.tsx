//?=== IMPORT CSS FILE ===?//
import Layout from "../../layout";
import "./index.css";
import { IReferenceCategory, IReferenceItem } from "../../interface";
import { Newsletter } from "../../components";
import { useEffect, useState } from "react";
import { getReference, getReferenceFilter } from "../../api/api";
import { HiOutlineEmojiSad } from "react-icons/hi";

export default function Reference() {
  useEffect(() => window.scrollTo({ top: 0, behavior: "smooth" }), []);

  const [category, setCategory] = useState<IReferenceCategory[] | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [isFilterLoad, setIsFilterLoad] = useState<boolean>(false);
  const [activeRegion, setActiveRegion] = useState<number>(2);
  const [items, setItems] = useState<null | IReferenceItem[]>(null);

  useEffect(() => {
    (async () => {
      const { data, success } = await getReference();
      if (success) {
        setCategory(data);
        setLoading(false);
      }
    })();
  }, []);

  useEffect(() => {
    (async () => {
      const { data } = await getReferenceFilter(`${activeRegion}`);
      setItems(data);
      setIsFilterLoad(false);
    })();
  }, [activeRegion]);

  const categoryFunc = (id: number) => {
    setActiveRegion(id);
    setIsFilterLoad(true);
  };

  return (
    <Layout>
      <div className="container">
        <div style={{ marginBlock: "5vh" }} className="category_products">
          <section>
            {loading ? (
              <div
                style={{
                  height: "50vh",
                  display: "grid",
                  placeContent: "center",
                }}
              >
                <div
                  className="spinner-border text-success"
                  role="status"
                ></div>
              </div>
            ) : (
              <>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    gap: "10px",
                  }}
                ></div>
                <h3>Отфильтровано по:</h3>
                <ul className="filter">
                  {category &&
                    category.map((item: IReferenceCategory) => (
                      <li
                        onClick={() => categoryFunc(item.id)}
                        style={{
                          whiteSpace: "nowrap",
                          color: activeRegion === item.id ? "#fff" : "",
                          opacity: activeRegion === item.id ? "1" : "",
                          fontWeight:
                            activeRegion === item.id ? "500" : "normal",
                          background:
                            activeRegion === item.id
                              ? "var(--light__green)"
                              : "",
                        }}
                        key={item.id}
                      >
                        {item.name}
                      </li>
                    ))}
                </ul>
                <div>
                  {isFilterLoad ? (
                    <div
                      style={{
                        height: "50vh",
                        display: "grid",
                        placeContent: "center",
                      }}
                    >
                      <div
                        className="spinner-border text-success"
                        role="status"
                      ></div>
                    </div>
                  ) : (
                    <div className="grid">
                      {items && items.length ? (
                        items.map((item) => (
                          <div className="grid_item">
                            <div className="grid_item_img">
                              <img
                                src={item.image}
                                alt={item.name}
                                loading="lazy"
                              />
                            </div>
                            <div className="grid_item_info">
                              <h4 className="title">{item.name}</h4>
                              <div className="grid_item_info_lists">
                                {Object.keys(item)[3] && (
                                  <p>
                                    <span>{Object.keys(item)[3]}:</span>{" "}
                                    {item["Площадь"]}
                                  </p>
                                )}

                                {Object.keys(item)[4] && (
                                  <p>
                                    <span>{Object.keys(item)[4]}:</span>{" "}
                                    {item["Места"]}
                                  </p>
                                )}

                                {Object.keys(item)[5] && (
                                  <p>
                                    <span>{Object.keys(item)[5]}:</span>{" "}
                                    {item["Соревнования"]}
                                  </p>
                                )}

                                {Object.keys(item)[6] && (
                                  <p>
                                    <span>{Object.keys(item)[6]}:</span>{" "}
                                    {item["Пожарная панель"]}
                                  </p>
                                )}

                                {Object.keys(item)[7] && (
                                  <p>
                                    <span>{Object.keys(item)[7]}:</span>{" "}
                                    {item["Извещатели"]}
                                  </p>
                                )}

                                {Object.keys(item)[8] && (
                                  <p>
                                    <span>{Object.keys(item)[8]}:</span>{" "}
                                    {item["Общее количество оборудования"]}
                                  </p>
                                )}
                              </div>
                            </div>
                          </div>
                        ))
                      ) : (
                        <h3>
                          <HiOutlineEmojiSad /> Не найдено
                        </h3>
                      )}
                    </div>
                  )}
                </div>
              </>
            )}
          </section>
        </div>
        <Newsletter />
      </div>
    </Layout>
  );
}
