import "./index.css";
import { Table } from "react-bootstrap";
import Layout from "../../layout";
import "bootstrap/dist/css/bootstrap.min.css";
import { MdDownload } from "react-icons/md";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getDocuments } from "../../api/api";
import { IDocs } from "../../interface";

export default function CategoryDocs() {
  const { category } = useParams();
  const [docs, setDocs] = useState<IDocs[] | []>([]);

  useEffect(() => {
    (async () => {
      const { success, data } = await getDocuments(category);
      success && setDocs(data);
    })();
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, [category]);

  console.log(category, docs);

  return (
    <>
      <Layout>
        <section className="table__wrapper" style={{ paddingTop: "20vh" }}>
          <div className="container">
            <Table striped bordered hover responsive className="documant_table">
              <thead>
                <tr>
                  <th style={{ width: "5%" }}>#</th>
                  <th>Имя</th>
                  <th style={{ textAlign: "center", width: "5%" }}>скачать</th>
                </tr>
              </thead>
              <tbody>
                {docs.map((item: IDocs, i: number) => (
                  <tr style={{ display: "flex !important" }} key={item.id}>
                    <th>{i + 1}</th>
                    <td style={{ whiteSpace: "nowrap" }}>{item.title}</td>
                    <td style={{ textAlign: "center" }}>
                      <a
                        href={item.file}
                        style={{ color: "inherit" }}
                        download
                        target="_blank"
                      >
                        <MdDownload
                          style={{ fontSize: "1.5rem", cursor: "pointer" }}
                        />
                      </a>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </section>
      </Layout>
    </>
  );
}
