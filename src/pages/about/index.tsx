import "./index.css";
import Layout from "../../layout";
import about_img from "../../assets/gst-about.jpg";
// import home_svg from "../../assets/home_svg.svg";
import { useEffect, useState } from "react";
import { Newsletter } from "../../components";
import { FaLocationArrow, FaPhone } from "react-icons/fa";
import { FaBuildingColumns, FaEarthAmericas } from "react-icons/fa6";
import { SiMetrodeparis } from "react-icons/si";
import { MdAttachEmail } from "react-icons/md";
import { Link } from "react-router-dom";

export default function About() {
  const [show, setShow] = useState<boolean>(true);
  const handleScroll = () => {
    const elem = document.querySelector(".about_2") as HTMLDivElement;
    elem.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(() => window.scrollTo({ top: 0, behavior: "smooth" }), []);

  return (
    <Layout>
      <section id="about" className="about">
        <div className="about__main">
          <h1 className="linear_gradient_title__light">
            Gulf Security Technology Co., Ltd.
          </h1>
          <div
            className="wrapper"
            style={{ alignItems: !show ? "center" : "start" }}
          >
            <div className="info">
              <p style={{ fontSize: "18px" }}>
                &nbsp;&nbsp;&nbsp;&nbsp;Компания Gulf Security Technology Co.,
                Ltd, GST, является ключевым игроком в сфере пожарной и охранной
                безопасности в Азии, а также надежным поставщиком комплексных
                решений для противопожарных систем по всему миру. Это дочерняя
                компания Carrier (Carrier Global Corporation), ведущего мирового
                поставщика инновационных технологий в области отопления,
                вентиляции и кондиционирования, охлаждения, технологий пожарной,
                охранной безопасности и автоматизации зданий. <br />
                &nbsp;&nbsp;&nbsp;&nbsp;GST предлагает широкий ассортимент
                противопожарной продукции и индивидуальные решения для
                противопожарных систем, адаптированные к потребностям различных
                отраслей промышленности. Имея инфраструктуру продаж, которая
                охватывает многие страны и регионы и включает в себя сеть из
                более чем 120 офисов продаж и несколько логистических центров по
                всему Китаю, GST стала важной частью глобального бизнеса
                Carrier.{" "}
                <span
                  style={{
                    marginLeft: "5px",
                    cursor: "pointer",
                    display: !show ? "none" : "inline-block",
                  }}
                  onClick={() => setShow((prev: boolean) => !prev)}
                >
                  ...
                </span>{" "}
                <br />
                <span style={{ display: !show ? "block" : "none" }}>
                  &nbsp;&nbsp;&nbsp;&nbsp;Чтобы удовлетворить потребности своей
                  международной клиентской базы, GST создала обширные
                  научно-исследовательские центры в Пекине и Циньхуандао на
                  севере Китая. Компания постоянно стремится разрабатывать новые
                  инновационные технологии и продукты. <br />
                  &nbsp;&nbsp;&nbsp;&nbsp;Завод GST в Циньхуандао — это крупная
                  производственная база, производящая интеллектуальные
                  электронные продукты, в которых используются инновационные
                  технологии. Для обеспечения превосходного качества продукции
                  компания располагает передовым производственным и контрольным
                  оборудованием, а также мощными возможностями в области
                  контроля качества и управления процессами.
                  <br />
                  &nbsp;&nbsp;&nbsp;&nbsp;Компания GST получила ряд национальных
                  и международных сертификатов соответствия стандартам, таким
                  как китайский CCC, международный UL, глобальный LPCB,
                  европейский CE и т.д.
                </span>
              </p>
              <div className="btns__info" style={{ marginBottom: "40px" }}>
                <button>
                  <Link
                    style={{ color: "inherit", textDecoration: "none" }}
                    to="/contacts"
                  >
                    Где купить
                  </Link>
                </button>
              </div>
            </div>
            <div className="images__wrapper">
              <img src={about_img} style={{ width: "480px" }} alt="Home" />
            </div>
          </div>
        </div>
      </section>

      <section className="about_2">
        <div className="arrow__container" onClick={handleScroll}>
          <div className="chevron"></div>
          <div className="chevron"></div>
          <div className="chevron"></div>
        </div>
        {/* <div className="container" style={{ marginTop: "4rem" }}>
          <div className="about_2_titles">
            <h3
              className="linear_gradient_title"
              style={{ textAlign: "center" }}
            >
              Что мы делаем?
            </h3>
            <p>
              Любой из наших специалистов ответит на все Ваши вопросы по
              системам пожарной безопасности и предложит наилучший вариант
              решения поставленной задачи. Фирма на сегодняшний день является
              одной из ведущих по консультации и сопровождению проектной
              документации по вопросам.
            </p>
          </div>
          <div className="documents">
            {aboutItems.map(({ id, title, desc, img }: ICard) => (
              <div className="box" key={id}>
                <div style={{ width: "100%" }}>
                  <LazyLoadImage src={img} alt={title} effect="blur" />
                </div>

                <p>{desc}</p>
              </div>
            ))}
          </div>
        </div> */}
      </section>

      <section className="map-wrapper">
        <div className="container">
          <div className="info-about">
            <h4>
              Официальный дистрибьютер продукции GST в Ташкенте компания
              «FireWise Enterprises»
            </h4>
            <div className="row">
              <div className="col">
                <div className="head">
                  <FaBuildingColumns className="icon" />
                  <h5>Название фирмы:</h5>
                </div>
                <span>ООО «FireWise Enterprises»</span>
              </div>
              <div className="col">
                <div className="head">
                  <FaLocationArrow className="icon" />
                  <h5>Адрес:</h5>
                </div>
                <span>
                  Ташкент, Юнусабадский район, <br /> 8 квартал, дом 35, кв. 14
                </span>
              </div>
              <div className="col">
                <div className="head">
                  <SiMetrodeparis className="icon" />
                  <h5>Метро</h5>
                </div>
                <span>Юнусабад Метро</span>
              </div>
              <div className="col">
                <div className="head">
                  <FaPhone className="icon" />
                  <h5>Телефон:</h5>
                </div>
                <span>
                  <a href="tel:+998 90 950-90-06">+998 90 950-90-06</a>
                  <br />
                  <a href="tel:+998 71 224-47-61">+998 71 224-47-61</a>
                  <br />
                  <a href="tel:+998 97 766 76 96">+998 97 766 76 96</a>
                  <br />
                  <a href="tel:+998 99 888 66 40">+998 99 888 66 40</a>
                </span>
              </div>
              <div className="col">
                <div className="head">
                  <MdAttachEmail className="icon" />
                  <h5>E-mail:</h5>
                </div>
                <span>
                  <a href="mailto:support@firewise.uz">support@firewise.uz</a>
                </span>
              </div>
              <div className="col">
                <div className="head">
                  <FaEarthAmericas className="icon" />
                  <h5>Website:</h5>
                </div>
                <span>
                  <a href="https://www.firewise.uz/">www.firewise.uz</a>
                </span>
              </div>
            </div>
          </div>
          <div className="map_styles">
            <h5 className="map_title">Схема проезда</h5>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m17!1m12!1m3!1d2994.2441956180337!2d69.28291471115344!3d41.36877897118252!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m2!1m1!2zNDHCsDIyJzA3LjYiTiA2OcKwMTcnMDcuOCJF!5e0!3m2!1suz!2s!4v1702217536904!5m2!1suz!2s"
              className="map_about"
              allowFullScreen={true}
              width={"720px"}
              height={"490px"}
            ></iframe>
          </div>
        </div>
      </section>
      <Newsletter />
    </Layout>
  );
}
