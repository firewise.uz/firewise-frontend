import "./index.css";
import Layout from "../../layout";
// import { IPostuserdata } from "../../interface";
import { useForm } from "react-hook-form";
import { useEffect } from "react";
import { postContact } from "../../api/api";
import { FaLocationArrow, FaPhone } from "react-icons/fa";
import { FaBuildingColumns } from "react-icons/fa6";
import { FaEarthAmericas } from "react-icons/fa6";
import { MdAttachEmail } from "react-icons/md";
import { SiMetrodeparis } from "react-icons/si";
import { toast } from "react-toastify";

type Inputs = {
  full_name: string;
  organization: string;
  phone_number: string;
  email: string;
  desc: string;
};

export default function Contacts() {
  useEffect(() => window.scrollTo({ top: 0, behavior: "smooth" }), []);
  const { register, handleSubmit, reset } = useForm<Inputs>();

  const onSubmit = async (values: Inputs) => {
    if (values.phone_number.length < 12) {
      toast.error("Введите свой полный номер телефона");
    } else {
      const { data, success } = await postContact(values);
      success && data && toast.success("Сообщения отправлены!");
      reset();
    }
  };

  // const handleCode = () => {
  //   if (userCode === actiteCode) {
  //     setDisabled(false);
  //     toast.success("Номер телефона подтвержден.");
  //     setIsPhoneActive(false);
  //   }
  // };

  // const handleActivePhone = async () => {
  //   if (fullName && organiz && tel) {
  //     setPhoneDisabled(true);
  //     const { success, data } = await checkPhoneNumber(tel);
  //     if (success) {
  //       setActive(data[tel]);
  //       toast.success("На номер телефона отправлен код подтверждения");
  //       setIsPhoneActive(true);
  //     }
  //   }
  // };

  return (
    <Layout>
      <section id="contacts" className="contacts">
        <div className="contacts-main">
          <div className="container">
            <div className="contact">
              <div className="contact_titles">
                <h1 className="linear_gradient_title__light">
                  ПРОФЕССИОНАЛИЗМ, НАДЕЖНОСТЬ И КАЧЕСТВО ВМЕСТЕ С НАМИ
                </h1>
              </div>
              <div className="images__wrapper"></div>
            </div>
          </div>
        </div>
      </section>

      <div className="container">
        <section id="contacts_2" className="contacts_2">
          <div className="info-about">
            <h4>
              Официальный дистрибьютер продукции «FireWise Enterprises» в
              Ташкент.
            </h4>
            <div className="row">
              <div className="col">
                <div className="head">
                  <FaBuildingColumns className="icon" />
                  <h5>Название фирмы:</h5>
                </div>
                <span>ООО «FireWise Enterprises»</span>
              </div>
              <div className="col">
                <div className="head">
                  <FaLocationArrow className="icon" />
                  <h5>Адрес:</h5>
                </div>
                <span>
                  Ташкент, Юнусабадский район,
                  <br /> 8 квартал, дом 35, кв. 14
                </span>
              </div>
              <div className="col">
                <div className="head">
                  <SiMetrodeparis className="icon" />
                  <h5>Метро</h5>
                </div>
                <span>Юнусабад Метро</span>
              </div>
              <div className="col">
                <div className="head">
                  <FaPhone className="icon" />
                  <h5>Телефон:</h5>
                </div>
                <span>
                  <a href="tel:+998 90 950-90-06">+998 90 950-90-06</a>
                  <br />
                  <a href="tel:+998 71 224-47-61">+998 71 224-47-61</a>
                  <br />
                  <a href="tel:+998 97 766 76 96">+998 97 766 76 96</a>
                  <br />
                  <a href="tel:+998 99 888 66 40">+998 99 888 66 40</a>
                </span>
              </div>
              <div className="col">
                <div className="head">
                  <MdAttachEmail className="icon" />
                  <h5>E-mail:</h5>
                </div>
                <span>
                  <a href="mailto:support@firewise.uz">support@firewise.uz</a>
                </span>
              </div>
              <div className="col">
                <div className="head">
                  <FaEarthAmericas className="icon" />
                  <h5>Website:</h5>
                </div>
                <span>
                  <a href="https://www.firewise.uz/">www.firewise.uz</a>
                </span>
              </div>
            </div>
          </div>
          <div className="contacts_right_sides">
            <div className="box_contact">
              <h4>Оставьте заявку</h4>
              <form onSubmit={handleSubmit(onSubmit)}>
                <input
                  type="text"
                  placeholder="ИМЯ ФАМИЛИЯ*"
                  {...register("full_name", { required: true })}
                  // onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  //   setFullName(e.target.value)
                  // }
                />
                <input
                  type="text"
                  placeholder="Организация*"
                  {...register("organization", { required: true })}
                  // onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  //   setOrganiz(e.target.value)
                  // }
                />
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    textAlign: "center",
                    gap: "10px",
                  }}
                  className="phoneNumberStyle"
                >
                  <input
                    type="tel"
                    placeholder="Телефон номерь*"
                    maxLength={13}
                    defaultValue={"+998"}
                    {...register("phone_number", {
                      required: true,
                    })}
                    // onChange={(e: ChangeEvent<HTMLInputElement>) =>
                    //   setTel(e.target.value)
                    // }
                  />
                </div>

                <input
                  type="text"
                  placeholder="Еmail*"
                  // disabled={disabled}
                  {...register("email", {
                    required: true,
                  })}
                  // style={{
                  //   cursor: disabled ? "no-drop" : "pointer",
                  //   opacity: disabled ? "0.6" : "1",
                  // }}
                />
                <textarea
                  cols={30}
                  rows={2}
                  // disabled={disabled}
                  // style={{
                  //   cursor: disabled ? "no-drop" : "pointer",
                  //   opacity: disabled ? "0.6" : "1",
                  // }}
                  placeholder="Вопрос*"
                  {...register("desc", {
                    required: true,
                  })}
                ></textarea>
                <button
                  type="submit"
                  // style={{
                  //   cursor: disabled ? "no-drop" : "pointer",
                  //   opacity: disabled ? "0.6" : "1",
                  // }}
                >
                  Отправить
                </button>
              </form>
            </div>
          </div>
        </section>
      </div>
    </Layout>
  );
}
