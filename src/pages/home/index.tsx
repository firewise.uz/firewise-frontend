//?=== IMPORT HOME CSS FILE ===?//
import "./index.css";
import { useEffect, useState } from "react";
import Layout from "../../layout";
import { ICard, INewProducts } from "../../interface";
import { Newsletter } from "../../components";
import { Link } from "react-router-dom";
// import home_svg from "../../assets/home_svg.svg";
import { getDocuments, getProducts } from "../../api/api";
import img from "../../assets/Journal-Entries.svg";
import { useLocation } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { aboutItems } from "../../constants";
import logo1 from "../../assets/logo-slide-1.png";
import logo2 from "../../assets/logo-slide-2.png";
import logo3 from "../../assets/logo-slide-3.png";
import logo4 from "../../assets/logo-slide-4.png";
import sertificateImage_1 from "../../assets/sertificate.jpg";
import sertificateImage_2 from "../../assets/sertificate-2.jpg";

const slider_images: string[] = [logo1, logo4, logo2, logo3];

const Home = () => {
  const [accordionActive, setAccordionActive] = useState<number>(1);
  const [docs, setDocs] = useState<string[]>([]);
  const [news, setNews] = useState<INewProducts[]>([]);
  const { hash } = useLocation();

  useEffect(() => {
    (async () => {
      const res = await getDocuments();
      res.success && setDocs(res?.data?.types);
      const res2 = await getProducts({ product_last_count: "3" });
      res2.success && setNews(res2.data);
    })();
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  useEffect(() => {
    if (hash.length) {
      const elem = document.querySelector(hash);
      setTimeout(() => {
        elem?.scrollIntoView({ behavior: "smooth" });
      }, 1500);
    }
  }, [hash]);

  const handleScroll = () => {
    const elem = document.querySelector(".home_2") as HTMLDivElement;
    elem.scrollIntoView({ behavior: "smooth" });
  };

  const ruDocsType: string[] = ["Документация", "Сертификаты", "Проекты"];

  return (
    <Layout>
      <section id="home" className="home">
        <div className="home__main">
          <div className="info">
            <h1 className="linear_gradient_title__light">
              ПРОФЕССИОНАЛИЗМ, НАДЕЖНОСТЬ И КАЧЕСТВО ВМЕСТЕ С НАМИ
            </h1>
            <p>
              ООО “FireWise Enterprises” сегодня – это полностью сформированная
              команда специалистов, осуществляющих деятельность в данном
              направлении с 2000-х годов. В состав фирмы входят: группа
              профессиональных консультантов, проектировщиков, монтажников и
              других специалистов. Любой из наших специалистов ответит на все
              Ваши вопросы по системам пожарной безопасности и предложит
              наилучший вариант решения поставленной задачи. Фирма на
              сегодняшний день является одной из ведущих по консультации и
              сопровождению проектной документации по вопросам обеспечения
              пожарной безопасноти при строительстве и действующих объектов, а
              также в оснащение любого объекта системами противопожарной защиты
              в Узбекистане
            </p>
            <div className="btns__info">
              <button>
                <Link
                  style={{ color: "inherit", textDecoration: "none" }}
                  to="/contacts"
                >
                  Где купить
                </Link>
              </button>
            </div>
          </div>
          <div className="images__wrapper">
            <img src={sertificateImage_1} alt="Home" />
            <img src={sertificateImage_2} alt="Home" />
          </div>
        </div>
      </section>

      <section className="home__bottom">
        <div className="slider-title">
          <p>Все наши партнеры, которые нас поддерживают</p>
        </div>
        <div className="logos">
          <div className="logos-slide">
            {[
              ...slider_images,
              ...slider_images,
              ...slider_images,
              ...slider_images,
              ...slider_images,
              ...slider_images,
              ...slider_images,
            ].map((img: string, i: number) => (
              <LazyLoadImage key={i} src={img} alt="logo" />
            ))}
          </div>
        </div>
      </section>

      <section
        className="home_2"
        // style={{ background: bgIsActive ? "#fff" : "var(--dark_green)" }}
      >
        <div className="arrow__container" onClick={handleScroll}>
          <div className="chevron"></div>
          <div className="chevron"></div>
          <div className="chevron"></div>
        </div>
      </section>
      {/* <section className="cards_wrapper">
        <div className="container">
          <div className="cards_two">
            <div className="cards_two_titles">
              <h2 className="linear_gradient_title__light">
                Все категории нашей продукции
              </h2>
              <p>Системы пожарной безопасности</p>
            </div>
            <div className="box_cards_two">
              {category?.map(({ title, id, image }: ICategory) => (
                <Link
                  to={`/products`}
                  key={id}
                  style={{ cursor: "pointer", textDecoration: "none" }}
                >
                  <div className="box">
                  <div className="box-image">
                    {image ? <LazyLoadImage style={{height:"330px", width:"100%"}} src={image} alt={title} effect="blur" />:null} 
                    </div>
                    <div className="box-title">
                    <p>{title}</p>
                    </div>
                  </div>
                </Link>
              ))}
            </div>
          </div>
        </div>
      </section> */}

      <div className="container" style={{ marginTop: "4rem" }}>
        <div className="about_2_titles">
          <h3 className="linear_gradient_title" style={{ textAlign: "center" }}>
            Что мы делаем?
          </h3>
          <p>
            Любой из наших специалистов ответит на все Ваши вопросы по системам
            пожарной безопасности и предложит наилучший вариант решения
            поставленной задачи. Фирма на сегодняшний день является одной из
            ведущих по консультации и сопровождению проектной документации по
            вопросам.
          </p>
        </div>
        <div className="documents">
          {aboutItems.map(({ id, title, desc, img }: ICard) => (
            <div className="box" key={id}>
              <div style={{ width: "100%" }}>
                <LazyLoadImage src={img} alt={title} effect="blur" />
              </div>

              <p>{desc}</p>
            </div>
          ))}
        </div>
      </div>

      <section className="accordion" style={{ paddingTop: "2rem" }}>
        <h2
          className="linear_gradient_title"
          style={{ marginBottom: "1rem", textAlign: "center" }}
        >
          О последних новостях
        </h2>
        <div className="container">
          <div className="accordion_image_gallery">
            {news.map((item: INewProducts, i: number) => (
              <div
                key={item.id}
                className={`accordion_item ${
                  i + 1 === accordionActive && "active"
                }`}
                onClick={() => setAccordionActive(i + 1)}
              >
                <div className="left">
                  <div className="accordion-header">
                    <span className="numbers">0/{i + 1}</span>
                    <span
                      className="title"
                      style={{
                        display: i + 1 !== accordionActive ? "block" : "none",
                      }}
                    >
                      {item.title.slice(0, 30)}...
                    </span>
                  </div>
                  <div className="accordion-body">
                    <div className="accordion-caption">
                      <h4>{item.title}</h4>
                      <p>{item.description.slice(0, 300)}...</p>
                    </div>
                    <div className="accordion-images">
                      {item.product_images[0]?.image ? (
                        <LazyLoadImage
                          // src={`${BASE_URL}${item.product_images[0].image}`}
                          src={`${item.product_images[0]?.image}`}
                          alt={item.title}
                          effect="blur"
                        />
                      ) : null}
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>

      <section id="documentation" className="feedback-wrapper">
        <div className="container">
          <div className="row_1">
            <div className="feedback-title">
              <h2 className="linear_gradient_title">
                Просмотр и скачивание документов
              </h2>
              <div className="documents">
                {docs.map((str: string, i: number) => (
                  <div
                    key={str}
                    className="box"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Link
                      to={`/documents/${str}`}
                      style={{ textDecoration: "none" }}
                    >
                      <img
                        style={{ width: "130px", height: "130px" }}
                        src={img}
                        alt="Documents image"
                      />
                      <h3>{ruDocsType[i]}</h3>
                      {/* <p>{desc}</p> */}
                    </Link>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <Newsletter />
      </section>
    </Layout>
  );
};

export default Home;
