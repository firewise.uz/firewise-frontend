/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react-hooks/exhaustive-deps */
//?=== IMPORT CSS FILE ===?//
import Layout from "../../layout";
import "./index.css";
import { INewProducts, ICategory } from "../../interface";
import { Newsletter } from "../../components";
import { Link, useLocation, 
  // useNavigate 
} from "react-router-dom";
import { useEffect, useState } from "react";
import { getCategorys, getProducts } from "../../api/api";
// import { BASE_URL } from "../../config";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { toast } from "react-toastify";

export default function ProductPage() {
  const location = useLocation();
  const params = new URLSearchParams(location.search).get("category");
  const [items, setItems] = useState<INewProducts[]>([]);
  const [filtered, setFiltered] = useState<ICategory[]>([]);
  // const [parentCategory,setParentCategory] = useState([]);
  const [active, setActive] = useState("");
  // const navigate = useNavigate();
  // const {state} = useLocation()
  // console.log(state);
  const countryid = localStorage.getItem("countryid")
  // const [isLoading, setIsLoading] = useState(true);
  const [isFilterLoad, setIsFilterLoad] = useState(true);
  const [isClicked,setIsclicked] = useState(false);
  // const [parentIdCategory,setParentIdCategory] = useState("");
  // const [activeButtonId, setActiveButtonId] = useState(null);

    // useEffect(() => {
    //   setIsFilterLoad(true);
    //   (async () => {
    //     const { data, success } = await getProducts({
    //       category_id: params as string,
    //     });
    //     if (success) {
    //       setIsFilterLoad(false);
    //       setItems(data);
    //     }
    //   })();
    // }, [params]);

    // useEffect(() => {
    //   setIsFilterLoad(true);
    //   setIsLoading(true);
    //   (async () => {
    //     const { data, success } = await getCategorys("");
    //     if (success) {
    //       setFiltered(data);
    //       setIsFilterLoad(false);
    //       const c = data.filter((c: ICategory) => c.id === +params!);
    //       params ? setActive(c[0].title) : setActive("Все");
    //       setIsLoading(false);
    //     }
    //   })();
    // }, []);
    // const fetchTypeCategory = async () => {
    //   try {
    //     const { data: typeCategories } = await axios.get("https://api.firewise.uz/typecategory/");
    //     console.log(typeCategories);
    //     setParentCategory(typeCategories)
    //     if (typeCategories.length > 0) {
    //       // Birinchi elementni aktivlashtirish
    //       // setParentIdCategorys();
    //     }
    //     // setParentIdCategorys(typeCategories[0]?.id)
    //    setIsclicked(true)
    //   } catch (error) {
    //     console.log(error);
        
    //   }
    // } 

    const fetchData = async () => {
      try {
        // Fetch type categories
        
        setIsFilterLoad(true);
        // Fetch categories based on parent category ID
        const { data: categoriesData, success } = await getCategorys(countryid);
        setIsclicked(false)
        if (success) {
         
          setFiltered(categoriesData);
          setIsFilterLoad(false);
  
          // Set active category based on params
          const defaultCategory = categoriesData.length > 0 ? categoriesData[0].title : "Все";
          setActive(defaultCategory);
          const c = categoriesData.find((c: ICategory) => c.id === +params!);
          // if (c) {
          //   setActive(c.title);
          // }
  
                  
          const categoryId = c ? c.id.toString() : categoriesData.length > 0 ? categoriesData[0].id.toString() : undefined;
           
         console.log(categoryId);
         
          if(categoryId){
            
            const { data: productsData, success: productsSuccess } = await getProducts({
              category_id: categoryId,
            });
    
            if (productsSuccess) {
              setIsFilterLoad(false);
              setItems(productsData);
              // setIsclicked(false)
            }
          }else{
            
            toast.error("Mahsulot yo'q")
          }
        }
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    // useEffect(()=>{
    //   fetchTypeCategory()
    // },[])
    useEffect(() => {
      fetchData();
    }, [params]);
    console.log(params);
    
    const getProduct = async(id:any,title:any) => {
      if(id){
            
        const { data: productsData, success: productsSuccess } = await getProducts({
          category_id: id,
        });

        if (productsSuccess) {
          setIsFilterLoad(false);
          setItems(productsData);
          setIsclicked(false)
          setActive(title)
          console.log(filtered[0].title);
          console.log(title);
          
          
        }
      }else{
        toast.error("Категория и продукты не найдены")
          setItems([])
        // toast.error("Mahsulot yo'q")
      }

    }

    // const setParentIdCategorys = async() => {
    //   // console.log(id);
    //   try {
    //     const {data:categoriesData} = await getCategorys(state.id)
    //     const c = categoriesData.find((c: ICategory) => c.id === +params!);
    //       // if (c) {
    //       //   setActive(c.title);
    //       // }
          
    //       // console.log(c);
          
    //     const categoryId = c ? c.id.toString() : categoriesData.length > 0 ? categoriesData[0].id.toString() : undefined;
    //     if(categoryId){
    //       console.log(categoryId);
          
    //       const { data: productsData, success: productsSuccess } = await getProducts({
    //         category_id: categoryId,
    //       });
         
  
    //       if (productsSuccess) {
    //         setIsFilterLoad(false);
    //         setItems(productsData);
    //         setIsclicked(false)
    //         console.log("salom");
    //         setActive(categoriesData[0].title)
    //       }
    //     }else{
    //       // categoryId
    //       toast.error("Категория и продукты не найдены")
    //       setItems([])
    //     }
        
    //     setFiltered(categoriesData)
    //     // setActive(categoriesData[0]?.title)
        
    //   } catch (error) {
    //     console.log(error);
        
    //   }
    //   // setParentIdCategory(state.id)
    //   setActiveButtonId(state.id);
      
    //    // Set the active button ID when clicked
    // };
  
    
    // console.log(parentIdCategory);
    

    

  useEffect(() => window.scrollTo({ top: 0, behavior: "smooth" }), []);

  return (
    <Layout>
      <div className="container">
        <div style={{ marginBlock: "5vh" }} className="category_products">
          <section>
            {isClicked ? (
              <div
                style={{
                  height: "50vh",
                  display: "grid",
                  placeContent: "center",
                }}
              >
                <div
                  className="spinner-border text-success"
                  role="status"
                ></div>
              </div>
            ) : (
              <>
              <div style={{display:"flex",alignItems:"center",justifyContent:"center",gap:"10px"}}>
              {/* {
                  parentCategory && parentCategory.map((item:any)=><div key={item.id}>
                  <button onClick={()=>setParentIdCategorys()} style={{
              padding: "10px 20px",
              backgroundColor: activeButtonId === item.id ? "#164234" : "initial",
              color: activeButtonId === item.id ? "white":"#000",
              border:"1px solid #B5B5B5",
              borderRadius:"20px"
               // Change background color conditionally
            }}>{item.title}</button>
                </div>
                )} */}
              </div>
                <h3>Отфильтровано по:</h3>
                <ul>
                  {/* <li
                    onClick={() => {
                      setActive("Все");
                      navigate(`/products`);
                    }}
                    style={{
                      whiteSpace: "nowrap",
                      background: active === "Все" ? "var(--light__green)" : "",
                      color: active === "Все" ? "#fff" : "",
                      opacity: active === "Все" ? "1" : "",
                      fontWeight: active === "Все" ? "500" : "normal",
                    }}
                  >
                    Все категории
                  </li> */}
                  {filtered.map((item: ICategory) => (
                    <li
                    onClick={() => {
                      setActive(item.title); // Set the title of the clicked category as active
                      // navigate(`/products?category=${item.id}`);
                      getProduct(item.id,item.title);
                    }}
                    style={{
                      whiteSpace: "nowrap",
                      color: active === item.title ? "#fff" : "",
                      opacity: active === item.title ? "1" : "",
                      fontWeight: active === item.title ? "500" : "normal",
                      background:
                        active === item.title ? "var(--light__green)" : "",
                    }}
                    key={item.id}
                  >
                    {item.title}
                  </li>
                  
                  ))}
                </ul>
                <div className="filtered_products" >
                  {isFilterLoad ? (
                    <div
                      style={{
                        height: "50vh",
                        display: "grid",
                        placeContent: "center",
                      }}
                    >
                      <div
                        className="spinner-border text-success"
                        role="status"
                      ></div>
                    </div>
                  ) : (
                    <>
                      {" "}
                      {items.map((item: INewProducts, i: number) => (
                        <Link
                          key={i}
                          to={`/products/${item.id}`}
                          style={{ color: "inherit", textDecoration: "none" }}
                        >
                          <div className="box_products">
                           {item.product_images[0]?.image ? <LazyLoadImage src={`${item.product_images[0]?.image}`} alt="Image" effect="blur"/>:null} 
                            <div
                              className="card_desc"
                              style={{
                                padding: "32px 22px 30px",
                                textAlign: "left",
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "flex-start",
                                justifyContent: "space-between",
                                height: "60%",
                              }}
                            >
                              <div>
                                <h3
                                  style={{
                                    marginBlock: "20px",
                                    fontSize: "20px",
                                    lineHeight: "24px",
                                    fontWeight: "500",
                                  }}
                                >
                                  {item.title}
                                </h3>
                                <p
                                  style={{
                                    fontSize: "16px",
                                    fontWeight: "400",
                                    lineHeight: "22px",
                                    marginBottom: "1rem",
                                    color: "#164234",
                                  }}
                                >
                                  {item.description.slice(0, 150)}
                                </p>
                              </div>
                              <button
                                style={{
                                  background: "transparent",
                                  border: "none",
                                  fontSize: "13px",
                                  fontWeight: "500",
                                  textTransform: "uppercase",
                                  lineHeight: "123%",
                                  letterSpacing: "2px",
                                  color: "#69c04b",
                                  marginTop: "30px",
                                }}
                              >
                                Подробнее
                              </button>
                            </div>
                          </div>
                        </Link>
                      ))}
                    </>
                  )}
                </div>
              </>
            )}
          </section>
        </div>
        <Newsletter />
      </div>
    </Layout>
  );
}
